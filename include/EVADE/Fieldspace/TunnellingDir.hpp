#pragma once

#include "EVADE/Fieldspace/Direction.hpp"

namespace EVADE {

/**
 * Status codes for the bounce. If the bounce has a negative value it will
 * correspond to one of these status codes.
 */
enum class BounceCode {
  uncalculated = -1, //!< The bounce has not yet been calculated, should never
                     //! appear in the output.
  initial = -2, //!< This point is the initial vacuum, no tunneling possible.
  initDegenerate = -3, //!< This point is degenerate with the initial vacuum, no
                       //! tunneling possible.
  notDeeper = -4,      //!< There is no deeper minimum in this direction.
  unbounded = -5       //!< The potential is unbounded in this direction.
};

namespace Fieldspace {

namespace IO {
//! Include the bounce if it is a EVADE::BounceCode
enum class PrintInvalidBounces { yes, no };
} // namespace IO

//! A Direction that includes a bounce value.
class TunnellingDir : public Direction {
public:
  /**
   * @brief Construct a new Tunnelling Dir object
   * @param B valid (positive) value for the Bounce
   * @param dir the direction
   */
  TunnellingDir(double B, Direction dir);

  /**
   * @brief Construct a new Tunnelling Dir object
   * @param B invalid BounceCode
   * @param dir the direction
   */
  TunnellingDir(BounceCode B, Direction dir);

  /**
   * @brief Get the bounce.
   * @return double
   */
  double B() const;

  //! Absolute tolerance for bounce comparison.
  static constexpr double atolB = 0.1;
  //! Relative tolerance for bounce comparison.
  static constexpr double rtolB = 1e-3;

  /**
   * @brief Checks if two TunnellingDir are approximately equal.
   * Checks that the bounce agrees within TunnellingDir:atolB and
   * TunnellingDir::rtolB and that the directions are approximately equal.
   * @param a first TunnellingDir
   * @param b second TunnellingDir
   * @return equal?
   */
  friend bool operator==(const TunnellingDir &a, const TunnellingDir &b);

  /**
   * @brief  Checks if a is smaller and not approximately equal to b.
   * Sorts first by bounce and second by the Direction
   * @param a first TunnellingDir
   * @param b second TunnellingDir
   * @return a<b?
   */
  friend bool operator<(const TunnellingDir &a, const TunnellingDir &b);

  /**
   * Converts a TunnellingDir into a string.
   * Includes the origin (optional) and target point.
   * @param invalidB       Include the bounce even if it is a EVADE::BounceCode.
   * @param  origin        Include the origin point.
   * @param  labels        Formats the output with labels and
   * braces around the field vector, else, simply outputs the values.
   * @param  precision          The floating point precision to use (matches
   * Point::rTolV).
   * @param  separator          The separator betweent the individual values.
   * @return                    A string representation of the FieldspacePoint.
   */
  std::string
  ToString(IO::PrintInvalidBounces invalidB = IO::PrintInvalidBounces::yes,
           IO::PrintOrigin origin = IO::PrintOrigin::no,
           IO::PrintLabels labels = IO::PrintLabels::yes, int precision = 7,
           const std::string &separator = ", ") const;

private:
  double B_;
};

/** @relates TunnellingDir
 * Stream operator to print a TunnellingDir.
 * Uses the default formatted TunnellingDir::ToString().
 * @param os  the output stream
 * @param obj the TunnellingDir to print
 * @return    the output stream
 */
std::ostream &operator<<(std::ostream &os, const TunnellingDir &obj);

/** @relates TunnellingDir
 * Canonical implementation in terms of operator==().
 */
inline bool operator!=(const TunnellingDir &a, const TunnellingDir &b) {
  return !(a == b);
}

/** @relates TunnellingDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator>(const TunnellingDir &a, const TunnellingDir &b) {
  return b < a;
}

/** @relates TunnellingDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator<=(const TunnellingDir &a, const TunnellingDir &b) {
  return !(b < a);
}

/** @relates TunnellingDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator>=(const TunnellingDir &a, const TunnellingDir &b) {
  return !(a < b);
}
} // namespace Fieldspace
} // namespace EVADE
