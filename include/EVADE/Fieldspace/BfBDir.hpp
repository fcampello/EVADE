#pragma once

#include "EVADE/Fieldspace/Direction.hpp"

namespace EVADE {
namespace Fieldspace {

struct BfBDir : public Direction {
  const double lam;
  static constexpr double atolLam = 1e-6;
  static constexpr double rtolLam = 1e-6;

  BfBDir(double lambda, Direction dir);
};

bool operator==(const BfBDir &a, const BfBDir &b);
bool operator<(const BfBDir &a, const BfBDir &b);

/** @relates BfBDir
 * Canonical implementation in terms of operator==().
 */
inline bool operator!=(const BfBDir &a, const BfBDir &b) { return !(a == b); }

/** @relates BfBDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator>(const BfBDir &a, const BfBDir &b) { return b < a; }

/** @relates BfBDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator<=(const BfBDir &a, const BfBDir &b) { return !(b < a); }

/** @relates BfBDir
 * Canonical implementation in terms of operator<().
 */
inline bool operator>=(const BfBDir &a, const BfBDir &b) { return !(a < b); }

} // namespace Fieldspace
} // namespace EVADE
