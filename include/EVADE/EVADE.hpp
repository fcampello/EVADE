#pragma once
/** @file
 * This header includes the other EVADE headers for the recommended settings.
 * You only need to manually include a header from `EVADE/Models/` for the
 * model you want to study.
 *
 * Using the convenience functions in this header a minimal use would be:
 * ```
 * #include "EVADE/EVADE.hpp"
 * #include "EVADE/Models/YourModel.hpp"
 *
 * void RunEVADE(const std::vector<std::vector<double>> &fieldSets,
 *               const std::vector<std::vector<double>> &parameterPoints) {
 *   using Model = EVADE::Models::YourModel;
 *   // initialize StationarityConditions object only once as this is slow
 *   auto statConds = EVADE::StationarityConditions<Model>{};
 *   // now loop over the parameter points
 *   for (const auto &parameters : parameterPoints) {
 *     const auto tunnelDirs = EVADE::CalculateTunnelling<Model>(
 *         EVADE::SolveForFieldSets(statConds, parameters, fieldSets),
 * parameters); if (EVADE::CheckStability(tunnelDirs[0])) std::cout <<
 * tunnelDirs[0];
 *   }
 * }
 * ```
 * The user only has to supply the fieldSets and the parameterPoints.
 * See the documentation of EVADE::FieldspacePoint for more information on what
 * data they contain and how to get at it.
 *
 */

#include "EVADE/BouncePolicies/Quartic.hpp"
#include "EVADE/Fieldspace/Point.hpp"
#include "EVADE/Fieldspace/TunnellingDir.hpp"
#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/SolverPolicies/Hom4ps2.hpp"
#include "EVADE/StationarityConditions.hpp"
#include "EVADE/TunnellingCalculator.hpp"

#include <algorithm>

namespace EVADE {

/**
 * Function to solve stationarity conditions for multiple field sets.
 * @tparam Model     EVADE model to use. Deduced from the statConds argument.
 * @param statConds  StationarityConditions instance.
 * Default constructed works fine. Note that construction of this is costly and
 * the instance is reusable for different parameter points.
 * @param parameters model parameters
 * @param fieldSets  the field sets as lists of the Model's field names
 * @returns          a sorted vector of unique stationary points
 */
template <class S>
std::vector<Fieldspace::Point>
SolveForFieldSets(S &statConds, const std::vector<double> &parameters,
                  const std::vector<std::vector<std::string>> &fieldSets) {
  std::vector<Fieldspace::Point> statpoints;
  for (const auto &fieldSet : fieldSets) {
    auto tempRes = statConds.SolveForFields(parameters, fieldSet);
    statpoints.insert(statpoints.end(), tempRes.begin(), tempRes.end());
  }
  std::sort(statpoints.begin(), statpoints.end());
  statpoints.erase(std::unique(statpoints.begin(), statpoints.end()),
                   statpoints.end());
  return statpoints;
}

/**
 * Function to calculate the bounce actions for the given stationary points.
 * @tparam Model     EVADE model to use. Has to be specified on call.
 * @param stationaryPoints  FieldspacePoint instances to consider.
 * @param parameters        model parameters
 * @returns                 a sorted vector of the tunnelling directions
 * connecting the initial vacuum of the model to each of the stationaryPoints
 */
template <class Model>
std::vector<Fieldspace::TunnellingDir>
CalculateTunnelling(std::vector<Fieldspace::Point> stationaryPoints,
                    const std::vector<double> &parameters) {
  TunnellingCalculator<Model> tunnel{parameters};
  std::vector<Fieldspace::TunnellingDir> result;
  for (auto &x : stationaryPoints) {
    result.emplace_back(tunnel.CalculateBounce(std::move(x)));
  }
  std::sort(result.begin(), result.end());
  return result;
}

/**
 * Which bound on the bounce action to use to judge the stability of a parameter
 * point.
 * The different cuts reflect the uncertainty from the unknown prefactor when
 * converting the bounce action to a lifetime as well as the uncertainties from
 * approximations int he boucne calculation.
 */
enum class StabilityConstraint {
  strict = 440,  //!< conservative constraint
  generous = 390 //!< only exclude if certainly unstable
};

/**
 * Check the stability of tunneling to the given point in fieldspace. Treats all
 * BounceCode values as stable.
 * @param direction   the point in fieldspace to tunnel to
 * @param constraint  the bound to use to judge the stability
 */
inline bool
CheckStability(const Fieldspace::TunnellingDir &direction,
               StabilityConstraint constraint = StabilityConstraint::generous) {
  return direction.B() < 0 || direction.B() > static_cast<double>(constraint);
}

} // namespace EVADE
