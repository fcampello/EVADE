#pragma once

#include <complex>
#include <string>
#include <vector>

namespace EVADE {
namespace Utilities {

/**
 * Checks whether a complex number is real up to numerical uncertainties.
 * Tuned to deal with the solutions of HOM4PS2.
 * @param  x a complex number
 * @return   whether \f$\Re(x) \gg \Im(x)\f$
 */
bool IsReal(const std::complex<double> &x);

/**
 * Checks if the given vector is real.
 * @param  vec complex input vektor
 * @return     IsReal for every element
 */
bool IsRealVector(const std::vector<std::complex<double>> &vec);

/**
 * Returns the real part of a comples vector.
 * @param  vec complex input vektor
 * @return     real part vector
 */
std::vector<double> RealPart(const std::vector<std::complex<double>> &vec);

double Norm(const std::vector<double> &vec);

/**
 * Normalizes a vector to 1.
 * I.e. scales the vector by a prefactor such that \f$\vec{\mathrm{vec}} \cdot
 * \vec{\mathrm{vec}}=1\f$.
 * @param vec the vector to normalize
 */
void Normalize(std::vector<double> &vec);

/**
 * Find the location of a string in a vector of strings.
 * @param  target  the string to search for
 * @param  storage the vector to search in
 * @return         index such that storage[index]==target
 */
size_t FindStringLocationIn(const std::string &target,
                            const std::vector<std::string> storage);

/**
 * Returns a copy of the path with any `//` replaced by `/`.
 * @param path input path
 */
std::string RemoveDoubleSlashes(std::string path);

/**
 * Looks for the target strings in the source vector and returns a list of
 * indices they can be found at.
 * @param  targets the target strings to look for
 * @param  source  the vector of strings the returned indices refer to
 * @return         the indices i such that source[i] is in target
 */
std::vector<size_t>
PositionsOfElementsIn(const std::vector<std::string> &targets,
                      const std::vector<std::string> &source);

std::vector<double> NormalizedDirection(const std::vector<double> &from,
                                        const std::vector<double> &to);

double Distance(const std::vector<double> &from, const std::vector<double> &to);

/**
 * Reinsert previously dropped fields which are zero.
 * @param reducedFields the non-zero Fields
 * @param nonZeroFields the indices of the non-zero fields in the original
 * fieldspace
 * @param nFields       the dimensionality of the original fieldspace
 */
std::vector<double> &
ReinsertZeroFields(std::vector<double> &reducedFields,
                   const std::vector<size_t> &nonZeroFields, size_t nFields);

} // namespace Utilities
} // namespace EVADE
