## EVADE

A c++-11 library to efficiently obtain constraints from vacuum stability in BSM
models with many scalar fields. The documentation for the library is available
at [jonaswittbrodt.gitlab.io/EVADE][doc].

### References

EVADE was developed by Jonas Wittbrodt in collaboration with Georg Weiglein and
Wolfgang G. Hollik. If you use EVADE for a publication please always cite
[1812.04644] and [1905.10234] and give reference to this website. [1812.04644]
contains a detailed explanation of the method used by EVADE to judge the
stability of the EW vacuum. 

### Usage

#### Compilation

The code compiles using cmake. If you cloned the repository and want to get
straight to using the code simply run

```bash
mkdir build && cd build
cmake ..
make -j
```

The example binaries are output to the `bin` and the libraries to the `lib`
subdirectory.

#### Dependencies

EVADE can use two different homptopy solvers: [HOM4PS2] and [Bertini]. Both of
these are downloaded and tested automatically by cmake. Since neither one
currently works reliably on Mac, EVADE can only be used on Linux.

The EVADE example binaries **require** the Config++ library. This can be
installed through the package manager on most linux distributions.

To generate documentation, EVADE uses Doxygen. If this _optional_ dependency is
available you can use the `make doc` target to generate html documentation. This
is the same as the documentation available [online][doc].

Some models (currently only the MSSM) support getting their model parameters
from an [SLHALib] data array. To use this _optional_ interface the SLHALib
header `SLHADefs.h` needs to be in your include path during compilation of EVADE
(and you of course have to link SLHAlib in your program).

#### Testing

EVADE includes a unit test suite that can be run with ctest.

#### Running

The provided executables accept configuration files as input and read parameters
from tabular files. As an example

```bash
./EVADE_MSSM example.cfg
```

Runs EVADE in the MSSM using default settings and a set of example parameter
points and saves the results to `testout.tsv`.

#### Linking

To link an external code to the library you need to link the `lib/libEVADE`
library and include the corresponding headers. When using cmake a simple
```
find_package(EVADE)
#...
target_link_libraries(yourTarget PRIVATE EVADE:EVADE)
```
should do the job.

A good place to start using EVADE is the `EVADE/EVADE.hpp` header file which
includes the recommended set of headers and provides convenience functions.

### Adding Models

The code for the models is generated using Mathematica and the corresponding
scripts in the `tools/Mathematica` directory, e.g. for the MSSM:

```Mathematica
<< GenerateModel.m
<< MSSM.m
generateMSSM[]
```

The content of the `Model.m` file defines all necessary input to generate a new
model. Only the scalar potential, the electroweak vacuum as well as variable and
field names are required.

<!-- Links -->

[1812.04644]: https://arxiv.org/abs/1812.04644
[1905.10234]: https://arxiv.org/abs/1905.10234
[hom4ps2]: http://www.math.nsysu.edu.tw/~leetsung/works/HOM4PS_soft_files/HOM4PS_Linux.htm
[bertini]: https://bertini.nd.edu/
[slhalib]: http://www.feynarts.de/slha/
[doc]: https://jonaswittbrodt.gitlab.io/EVADE
