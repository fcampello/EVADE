#pragma once
/** @file */

#include <cstddef>
#include <fstream>
#include <string>
#include <vector>

namespace EVADE {
namespace Fieldspace {
class TunnellingDir;
class Point;
} // namespace Fieldspace
} // namespace EVADE

/**
 * Specifies the type of output.
 * @elem stability Outputs the most important information in a compressed form.
 * If the Model has one fields called `x1` the file would look something like:
 * ```
 * index  init_V init_x1 fast_B fast_V fast_x1 deep_B deep_V deep_x1
 * point0 -1000. 240.0   39.0   -3e5   2200.1  1200.  -6e6   1700.
 * point1 ...
 * ```
 * with each whitespace replaced by a single `\t`.
 * The `init_` columns refer to the initial vacuum of the point, the `fast_` to
 * the point with the lowest bounce action and the `deep_` to the one with the
 * lowest value of the potential.
 * @elem vevs     Outputs all considered stationary points one per line, where
 * only the index in the first column specifies the related parameter point.
 */
enum class OutputType { stability, vevs };

/**
 * Prints the results of the stability calculations to a file.
 * The form of the output depends on the OutputType.
 *
 */
class StabilityOutput {
public:
  /**
   * Constructs an output object which writes to the given file.
   * It also writes a header row using the fieldnames.
   * @param filepath   the file to write to
   * @param fieldnames names of the fields
   */
  StabilityOutput(const std::string &filepath, OutputType type,
                  const std::vector<std::string> &fieldnames);

  /**
   * Writes the interesting Tunnelling Directions from the results.
   * @param pointID the ID to relate this output to the input
   * @param results the vector of TunnellingDir with calculated bounces
   */
  void Write(const std::string &pointID,
             const std::vector<EVADE::Fieldspace::TunnellingDir> &results);

private:
  std::ofstream file_;
  OutputType type_;

  void HeaderStability(const std::vector<std::string> &fieldnames);
  void
  WriteStability(const std::string &pointID,
                 const std::vector<EVADE::Fieldspace::TunnellingDir> &results);

  void HeaderVevs(const std::vector<std::string> &fieldnames);
  void WriteVevs(const std::string &pointID,
                 const std::vector<EVADE::Fieldspace::TunnellingDir> &results);
};

/** @relates StabilityOutput
 * Find the FieldspacePoint with the deepest potential.
 * This is massively inefficient, but that should not be an importand issue.
 * @param  points the points to look in
 * @return        reference to the deepest among the points
 */
const EVADE::Fieldspace::TunnellingDir &
GetDeepest(const std::vector<EVADE::Fieldspace::TunnellingDir> &points);

/** @relates StabilityOutput
 * Find the FieldspacePoint with the fastest tunneling.
 * @param  points the points to look in
 * @return        reference to the fastest among the points
 */
const EVADE::Fieldspace::TunnellingDir &
GetFastest(const std::vector<EVADE::Fieldspace::TunnellingDir> &points);

/**
 * Counts the number of points, the number of deeper points and the number of
 * points degenerate with the initial vacuum.
 * @param  points the points to look in
 * @return        {# of points, # of deeper points, # of degenerate points}
 */
std::vector<size_t>
CountStatistics(const std::vector<EVADE::Fieldspace::TunnellingDir> &points);
