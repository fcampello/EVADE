#pragma once

#include <chrono>
#include <iostream>

class ProgressBar {
private:
  static constexpr int barWidth = 70;

  const size_t totalTicks;

  size_t ticks = 0;

  const std::chrono::steady_clock::time_point startTime =
      std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point lastUpdate =
      std::chrono::steady_clock::now();

  void Update(const std::chrono::steady_clock::time_point &now,
              bool returnLine = true) {
    float progress = static_cast<float>(ticks) / static_cast<float>(totalTicks);
    int pos = static_cast<int>(barWidth * progress);
    float runtime = static_cast<float>(
        std::chrono::duration_cast<std::chrono::milliseconds>(now - startTime)
            .count());

    std::cout << "[";
    for (int i = 0; i < barWidth; ++i) {
      if (i < pos) {
        std::cout << '=';
      } else if (i == pos) {
        std::cout << '>';
      } else {
        std::cout << ' ';
      }
    }
    std::cout << "] " << static_cast<int>(progress * 100.0) << "% "
              << runtime / 1000.0 << "s";
    if (returnLine) {
      std::cout << "\r";
    }
    std::cout.flush();
  }

  void CheckUpdate() {
    auto now = std::chrono::steady_clock::now();

    if (std::chrono::duration_cast<std::chrono::milliseconds>(now - lastUpdate)
            .count() > 200) {
      lastUpdate = now;
      Update(now);
    }
  }

public:
  ProgressBar(size_t total) : totalTicks{total} {}

  size_t operator++() {
    ++ticks;
    if (ticks > totalTicks) {
      ticks = totalTicks;
    }
    CheckUpdate();
    return ticks;
  }

  ~ProgressBar() {
    Update(std::chrono::steady_clock::now(), false);
    std::cout << "\n";
  }
};
