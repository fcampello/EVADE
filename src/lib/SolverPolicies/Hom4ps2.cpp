#include "EVADE/SolverPolicies/Hom4ps2.hpp"

#include "EVADE/Utilities/Polynomial.hpp"
#include "EVADE/Utilities/Utilities.hpp"
#include "EVADE/config.h"

#include <complex>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>

EVADE::Solver::Hom4ps2::Hom4ps2(const std::string &path, HomType homtype)
    : folder_{path + "/HOM4PS2_"},
      command_{"cd " + folder_.Path() + ";" + "echo " +
               std::to_string(static_cast<int>(homtype)) + " | " +
               "./hom4ps2 evade.sym >/dev/null"} {
  folder_.MakeSubfolder("bin");
  std::string linkpath(HOM4PS2_EXECUTABLE);
  folder_.MakeSymlink(linkpath, "hom4ps2"),
      linkpath.erase(linkpath.rfind("hom4ps2"), std::string::npos);
  folder_.MakeSymlink(linkpath + "bin/flwcrv", "bin/flwcrv");
  folder_.MakeSymlink(linkpath + "bin/sym2num", "bin/sym2num");
  folder_.SetCleanup({"evade.sym", "data.roots", "bin/input.num"});
}

std::vector<std::vector<double>> EVADE::Solver::Hom4ps2::Call(
    const std::vector<Utilities::Polynomial> &equations,
    const std::vector<std::string> &varnames) {
  WriteInput(equations, varnames);
  Run();
  try {
    return ReadResult(varnames);
  } catch (Hom4ps2Blowup &) {
    Run();
    return ReadResult(varnames, OnBlowup::ignore);
  } catch (Hom4ps2ReadError &) {
    Run();
    return ReadResult(varnames);
  }
}

void EVADE::Solver::Hom4ps2::WriteInput(
    const std::vector<Utilities::Polynomial> &equations,
    const std::vector<std::string> &varnames) {
  std::ofstream ofile;
  ofile.open(folder_.Path() + std::string("evade.sym"));
  ofile << "{\n";
  for (const auto &eq :
       equations) { // iterate over equations, each one gets a row
    ofile << eq.ToString(
        varnames, std::numeric_limits<double>::max_digits10,
        "^"); // format polynomial for HOM4PS2 with maximum precision
    ofile << ";" << std::endl; // finish the equation and line
  }
  ofile << "}" << std::endl; // finish the file
  ofile.close();
}

void EVADE::Solver::Hom4ps2::Run() {
  int status = std::system(command_.c_str());
  if (status != 0) {
    throw(Hom4ps2RunError(status, folder_, command_));
  }
}

std::vector<std::vector<double>>
EVADE::Solver::Hom4ps2::ReadResult(const std::vector<std::string> &varnames,
                                   OnBlowup onBlowup) {
  std::vector<std::vector<double>> result;
  std::ifstream ifile(folder_.Path() + std::string("data.roots"));
  if (ifile) {
    std::string temp;

    // first go to where the order of variables is written
    while (getline(ifile, temp)) {
      if (temp.compare("  The order of variables : ") == 0) {
        break;
      }
    }
    // extract order of variables
    std::vector<size_t> order;
    while (ifile >> temp && temp.compare("===============>") != 0) {
      order.push_back(Utilities::FindStringLocationIn(temp, varnames));
    }
    // get the total number of solutions and the number of real solutions
    while (ifile >> temp && temp.compare("=") != 0) { // jump to next single =
    }
    size_t n_roots;
    ifile >> n_roots;
    while (ifile >> temp && temp.compare("=") != 0) { // jump to next single =
    }
    size_t n_r_roots;
    ifile >> n_r_roots;
    while (ifile >> temp && temp.compare("=") != 0) { // jump to next single =
    }
    size_t n_paths;
    ifile >> n_paths;
    while (ifile >> temp && temp.compare("=") != 0) { // jump to next single =
    }
    size_t n_blow;
    ifile >> n_blow;

    if (n_blow > 0 && onBlowup == OnBlowup::throwEx) {
      throw Hom4ps2Blowup{};
    }

    // create point buffer
    std::vector<std::complex<double>> point(varnames.size(), 0);
    // go back to beginning
    ifile.seekg(0, ifile.beg);

    size_t n_read = 0;
    while (ifile >> point.at(order[0])) {
      // read remainder of point into buffer
      for (size_t i = 1; i < order.size(); ++i) {
        ifile >> point.at(order[i]);
      }
      // if point is real write it to a vev configuration...
      if (Utilities::IsRealVector(point)) {
        result.push_back(Utilities::RealPart(point));
      }
      // skip over the remaining lines for this point
      while (ifile >> temp) {
        if (temp.compare("---------------------------------------") == 0) {
          break;
        }
      }
      n_read++;
    }
    ifile.close();
    if (n_read != n_roots || result.size() < n_r_roots) {
      throw(Hom4ps2ReadError(folder_, n_read, result.size()));
    }
  } else {
    std::cerr << "HOM4PS2 result file could not be opened!" << std::endl;
  }
  return result;
}

EVADE::Solver::Hom4ps2Blowup::Hom4ps2Blowup()
    : std::runtime_error{"Blowup in Hom4ps2"} {}

EVADE::Solver::Hom4ps2RunError::Hom4ps2RunError(
    int exitStatus, EVADE::Utilities::TempDir &folder,
    const std::string &callCommand)
    : std::runtime_error{
          "HOM4PS2 returned non-zero exit status: " +
          std::to_string(exitStatus) + ". Check input file " + folder.Path() +
          "evade.sym. HOM4PS2 was called using: " + callCommand} {
  folder.DontClean("evade.sym");
}

EVADE::Solver::Hom4ps2ReadError::Hom4ps2ReadError(
    EVADE::Utilities::TempDir &folder, size_t nTot, size_t nReal)
    : std::runtime_error{"Could not read all roots from HOM4PS2. " +
                         std::to_string(nReal) + "/" + std::to_string(nTot) +
                         " of the read roots are real."
                         "Check output file " +
                         folder.Path() + "data.roots."} {
  folder.DontClean("data.roots");
}
