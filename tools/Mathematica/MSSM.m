name = "MSSM";

su2prod[x_List, y_List] := x[[1]]*y[[2]] - x[[2]] y[[1]]
W = mu su2prod[Hu, Hd] + yt su2prod[Q, Hu] Conjugate[tr] +
    yb su2prod[Hd, Q] Conjugate[br] +
    yl su2prod[Hd, L] Conjugate[lr] /. {Hu -> {Hup, Hu0},
    Hd -> {Hd0, Hdm}, Q -> {tl, bl}, L -> {vl, ll}};
F = Total[
   Flatten[Table[
     D[W, x] Conjugate[D[W, x]], {x, {Hu0, Hd0, Hup, Hdm, tl, bl, vl,
       ll, Conjugate[tr], Conjugate[br], Conjugate[lr]}}]]];
D2 = Total[
    Flatten[Table[(2 (ConjugateTranspose[x1].x2) (ConjugateTranspose[
             x2].x1) - (ConjugateTranspose[
             x1].x1) (ConjugateTranspose[x2].x2)), {x1, {Hu, Hd, Q,
         L}}, {x2, {Hu, Hd, Q, L}}] /. {Hu -> {{Hup}, {Hu0}},
       Hd -> {{Hd0}, {Hdm}}, Q -> {{tl}, {bl}}, L -> {{vl}, {ll}}}]];
V0 = (F
     + g1^2/
       8 (Abs[Hu0]^2 + Abs[Hup]^2 - Abs[Hd0]^2 - Abs[Hdm]^2 +
         1/3 Abs[bl]^2 + 2/3 Abs[br]^2 + 1/3 Abs[tl]^2 -
         4/3 Abs[tr]^2 - Abs[ll]^2 + 2 Abs[lr]^2 - Abs[vl]^2)^2
     + g2^2/8 D2
     + g3^2/6 (Abs[tl]^2 -  Abs[tr]^2 + Abs[bl]^2 - Abs[br]^2)^2
     + mHusq (Abs[Hu0]^2 + Abs[Hup]^2)
     + mHdsq (Abs[Hd0]^2 + Abs[Hdm]^2)
     + mL3^2 (Abs[ll]^2 + Abs[vl]^2)
     + mE3^2 Abs[lr]^2
     + mQ3^2 (Abs[tl]^2 + Abs[bl]^2)
     + mTOP^2  Abs[tr]^2
     + mBOT^2 Abs[br]^2
     + (mAsq*Sin[beta]*Cos[beta]*su2prod[Hu, Hd] +
       Conjugate[mAsq]*Sin[beta]*Cos[beta]*
        su2prod[Conjugate[Hu], Conjugate[Hd]])
     + Conjugate[tr] Tt (Hu0 tl - Hup bl) +
     Conjugate[Conjugate[tr] Tt (Hu0 tl - Hup bl)]
     + Conjugate[br] Tb (Hd0 bl - Hdm tl) +
     Conjugate[Conjugate[br] Tb (Hd0 bl - Hdm tl)]
     + Conjugate[lr] Tl (Hd0 ll - Hdm vl)  +
     Conjugate[Conjugate[lr] Tl (Hd0 ll - Hdm vl)]
    ) /. {Hu -> {Hup, Hu0}, Hd -> {Hd0, Hdm}};
norm = Sqrt[2];
slhapars = {mu -> SLHA::HMIX[1], v0 -> SLHA::HMIX[3],
   mAsq -> SLHA::HMIX[4], beta -> SLHA::HMIX[10],
   g1 -> SLHA::GAUGE[1], g2 -> SLHA::GAUGE[2], g3 -> SLHA::GAUGE[3],
   mHusq -> SLHA::MSOFT[22], mHdsq -> SLHA::MSOFT[21],
   yt -> SLHA::YU[3, 3], yb -> SLHA::YD[3, 3], yl -> SLHA::YE[3, 3],
   mQ3^2 -> SLHA::MSQ2[3, 3], mTOP^2 -> SLHA::MSU2[3, 3],
   mBOT^2 -> SLHA::MSD2[3, 3], mL3^2 -> SLHA::MSL2[3, 3],
   mE3^2 -> SLHA::MSE2[3, 3], Tt -> SLHA::TU[3, 3],
   Tb -> SLHA::TD[3, 3], Tl -> SLHA::TE[3, 3]};
V = ComplexExpand[
   V0 /. slhapars /. {Hup -> hup/norm, Hdm -> hdm/norm,
      Hu0 -> hu0/norm, Hd0 -> hd0/norm, bl -> bl/norm,
      br -> br/norm, tl -> tl/norm, tr -> tr/norm, ll -> ll/norm,
      lr -> lr/norm, vl -> vl/norm} /. {hup -> vhurp + I vhuip,
     hdm -> vhdrm + I vhdim, hu0 -> vhur0 + I vhui0,
     hd0 -> vhdr0 + I vhdi0, tl -> vulr3 + I vuli3,
     tr -> vurr3 + I vuri3, bl -> vdlr3 + I vdli3,
     br -> vdrr3 + I vdri3, ll -> velr3 + I veli3,
     lr -> verr3 + I veri3, vl -> vvlr3 + I vvli3}];

ewvacuum = {vhur0 -> Sin[beta]*v0 + vhur0, vhdr0 -> Cos[beta]*v0 + vhdr0} /. slhapars;
fields = {vhur0, vhui0, vhdr0, vhdi0, vhurp, vhuip, vhdrm, vhdim,
  vulr3, vuli3, vurr3, vuri3, vdlr3, vdli3,  vdrr3, vdri3, velr3,
  veli3, verr3, veri3, vvlr3, vvli3};
parameters = {SLHA::GAUGE[1], SLHA::GAUGE[2], SLHA::GAUGE[3],
  SLHA::HMIX[1], SLHA::HMIX[3], SLHA::HMIX[4], SLHA::HMIX[10],
  SLHA::YU[3, 3], SLHA::YD[3, 3], SLHA::YE[3, 3], SLHA::MSOFT[21],
  SLHA::MSOFT[22], SLHA::MSQ2[3, 3], SLHA::MSU2[3, 3],
  SLHA::MSD2[3, 3], SLHA::MSL2[3, 3], SLHA::MSE2[3, 3],
  SLHA::TU[3, 3], SLHA::TD[3, 3], SLHA::TE[3, 3]};

  SLHALibPars = {"Gauge_g1.real()", "Gauge_g2.real()", "Gauge_g3.real()", "HMix_MUE.real()",
    "HMix_VEV.real()", "HMix_MA02.real()", "std::atan(MinPar_TB.real())", "Yu_Yt.real()", "Yd_Yb.real()",
    "Ye_Ytau.real()", "MSoft_MHd2.real()", "MSoft_MHu2.real()", "MSQ2_MSQ2(3,3).real()", "MSU2_MSU2(3,3).real()",
    "MSD2_MSD2(3,3).real()", "MSL2_MSL2(3,3).real()", "MSE2_MSE2(3,3).real()", "Tu_Tf(3,3).real()",
    "Td_Tf(3,3).real()", "Te_Tf(3,3).real()"}

generateMSSM[]:= generateModel[name,V,fields,parameters,ewvacuum,
                               parnameRules->SlhaVarnameRule,
                               slhalibpars->SLHALibPars]
