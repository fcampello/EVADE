#include "EVADE/EVADE.hpp"
#include "EVADE/Models/Dummy.hpp"
#include <iostream>

namespace {
const std::vector<std::vector<double>> parameterPoints(10);
const std::vector<std::vector<std::string>> fieldSets{"x0", "x1"};
} // namespace

int main() {
  using Model = EVADE::Models::Dummy;
  // initialize StationarityConditions object only once as this is slow
  auto statConds = EVADE::StationarityConditions<Model>{};
  // now loop over the parameter points
  for (const auto &parameters : parameterPoints) {
    const auto tunnelDirs = EVADE::CalculateTunnelling<Model>(
        EVADE::SolveForFieldSets(statConds, parameters, fieldSets), parameters);
    if (EVADE::CheckStability(tunnelDirs[0]))
      std::cout << tunnelDirs[0];
  }
}
