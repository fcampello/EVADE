#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/Utilities/Polynomial.hpp"

#include "catch.hpp"

#include <stdexcept>
#include <vector>

TEST_CASE("Generic Scaling works", "[unit][gscaling]") {
  using EVADE::Scaling::Generic;
  using EVADE::Utilities::Polynomial;

  Generic scale;

  Polynomial eq1;
  eq1[{1, 1, 1}] = -1.;
  eq1[{2, 3, 1}] = -1.;
  eq1[{0, 2, 0}] = 1.;

  Polynomial eq2;
  eq2[{2, 1, 0}] = -1.;
  eq2[{2, 0, 1}] = -2.;
  eq2[{2, 2, 0}] = 1.;

  Polynomial eq3;
  eq3[{2, 1, 0}] = -1.;
  eq3[{2, 0, 1}] = -2.;
  eq3[{2, 2, 0}] = 1.;

  std::vector<Polynomial> system1{eq1, eq2};
  std::vector<Polynomial> system2{eq1, eq2, eq3};

  REQUIRE_THROWS_AS(scale.ScaleSystem(system1), std::out_of_range);
  REQUIRE_NOTHROW(scale.ScaleSystem(system2));
}
