#include "EVADE/SolverPolicies/Hom4ps2.hpp"
#include "EVADE/Utilities/Polynomial.hpp"
#include "catch.hpp"

#include <algorithm>
#include <cmath>

using namespace EVADE;
using EVADE::Utilities::Polynomial;

TEST_CASE("HOM4PS2", "[unit][hom4ps2]") {
  Polynomial eq1;

  eq1[{2, 0}] = 1.;
  eq1[{1, 1}] = -2.;
  eq1[{0, 2}] = -1.;
  eq1[{0, 0}] = 4.;

  Polynomial eq2;

  eq2[{2, 0}] = 1.;
  eq2[{1, 1}] = -3.;
  eq2[{0, 2}] = 2.;

  std::vector<std::string> varnames{"x1", "x2"};
  SECTION("solve simple example with linear homotopy") {
    Solver::Hom4ps2 hom4ps2(".", Solver::Hom4ps2::HomType::linear);

    std::vector<std::vector<double>> result =
        hom4ps2.Call({eq1, eq2}, varnames);

    std::sort(result.begin(), result.end());

    REQUIRE(result[0][0] == CatchApprox(-4.));
    REQUIRE(result[0][1] == CatchApprox(-2.));

    REQUIRE(result[1][0] == CatchApprox(-sqrt(2.)));
    REQUIRE(result[1][1] == CatchApprox(-sqrt(2.)));

    REQUIRE(result[2][0] == CatchApprox(sqrt(2.)));
    REQUIRE(result[2][1] == CatchApprox(sqrt(2.)));

    REQUIRE(result[3][0] == CatchApprox(4.));
    REQUIRE(result[3][1] == CatchApprox(2.));
  }

  SECTION("solve simple example with polyhedral homotopy") {
    Solver::Hom4ps2 hom4ps2(".", Solver::Hom4ps2::HomType::polyhedral);

    std::vector<std::vector<double>> result =
        hom4ps2.Call({eq1, eq2}, varnames);

    std::sort(result.begin(), result.end());

    REQUIRE(result[0][0] == CatchApprox(-4.));
    REQUIRE(result[0][1] == CatchApprox(-2.));

    REQUIRE(result[1][0] == CatchApprox(-sqrt(2.)));
    REQUIRE(result[1][1] == CatchApprox(-sqrt(2.)));

    REQUIRE(result[2][0] == CatchApprox(sqrt(2.)));
    REQUIRE(result[2][1] == CatchApprox(sqrt(2.)));

    REQUIRE(result[3][0] == CatchApprox(4.));
    REQUIRE(result[3][1] == CatchApprox(2.));
  }

  SECTION("invalid input throws") {
    Solver::Hom4ps2 hom4ps2(".", Solver::Hom4ps2::HomType::linear);

    REQUIRE_THROWS_AS(hom4ps2.Call({eq1}, varnames), Solver::Hom4ps2RunError);
  }
}
