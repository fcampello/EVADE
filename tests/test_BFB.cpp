#include "EVADE/Models/Dummy.hpp"
#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/SolverPolicies/Hom4ps2.hpp"
#include "EVADE/StationarityConditions.hpp"
#include "EVADE/Utilities/Polynomial.hpp"
#include "EVADE/Utilities/Utilities.hpp"
#include "catch.hpp"

#include <algorithm>

TEST_CASE("unit sphere", "[unit][utils]") {
  using namespace EVADE;

  REQUIRE(Utilities::UnitSphere(5).size() == 6);
  REQUIRE(Utilities::UnitSphere(1).size() == 2);

  auto statConds = Utilities::DivergenceForNonZero(
      Models::Dummy::VPoly4({}),
      Utilities::PositionsOfElementsIn({"x0", "x1"},
                                       Models::Dummy::FieldNames()));

  for (const auto &x : statConds) {
    WARN(x);
  }
  REQUIRE(statConds[0].size() == 3);
  REQUIRE(statConds[1].size() == 4);
  auto sizeSort = [](const Utilities::Polynomial &a,
                     const Utilities::Polynomial &b) -> bool {
    return a.size() < b.size();
  };
  *std::max_element(statConds.begin(), statConds.end(), sizeSort) =
      Utilities::UnitSphere(statConds[0].VarCount());

  for (const auto &x : statConds) {
    WARN(x);
  }
  REQUIRE(statConds[0].size() == 3);
  REQUIRE(statConds[1].size() == 3);
}

// TEST_CASE("BfB", "[unit][dummy][bfb]") {
//   using namespace EVADE;

//   EVADE::StationarityConditions<EVADE::Models::Dummy> stat;

//   auto bfb = stat.BfBForFields({}, {"x0", "x1"});
//   std::sort(bfb.begin(), bfb.end());
//   for (const auto &x : bfb) {
//     WARN(x);
//   }
//   REQUIRE(bfb[0].GetPot() == Catch::Detail::Approx(-2.84306));
// }
