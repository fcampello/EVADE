#include "EVADE/BouncePolicies/Quartic.hpp"
#include "EVADE/Fieldspace/Point.hpp"
#include "EVADE/Fieldspace/TunnellingDir.hpp"
#include "EVADE/Models/Dummy.hpp"
#include "EVADE/Models/MSSM.hpp"
#include "EVADE/TunnellingCalculator.hpp"
#include "catch.hpp"

using namespace EVADE;

TEST_CASE("Calculate tunneling for Dummy", "[unit][tunneling]") {
  TunnellingCalculator<Models::Dummy, Bounce::Quartic> test({});

  std::vector<Fieldspace::Point> testpoints{
      Fieldspace::Point(
          Models::Dummy::InitialVacuum({}),
          Models::Dummy::V({}, Models::Dummy::InitialVacuum({}))), // initial
      Fieldspace::Point({1., 0.}, -1000),                          // degenerate
      Fieldspace::Point({100., 2.}, -10000), // deeper and valid
      Fieldspace::Point({-100., 2.}, 100)    // not deeper
  };

  std::vector<Fieldspace::TunnellingDir> result;
  for (auto &x : testpoints) {
    result.emplace_back(test.CalculateBounce(x));
  }
  INFO(Models::Dummy::V({}, Models::Dummy::InitialVacuum({})));

  REQUIRE(result[0].B() ==
          CatchApprox(static_cast<double>(BounceCode::initial)));
  REQUIRE(result[1].B() ==
          CatchApprox(static_cast<double>(BounceCode::initDegenerate)));
  REQUIRE(result[2].B() == CatchApprox(990.884));
  REQUIRE(result[3].B() ==
          CatchApprox(static_cast<double>(BounceCode::notDeeper)));
}

TEST_CASE("Calculate tunneling MSSM", "[unit][tunneling][MSSM]") {
  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      -7155.0,             // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };
  TunnellingCalculator<Models::MSSM, Bounce::Quartic> test(testpars);
  Fieldspace::Point testpoint1({36873.5, 0, 41169.1,  0, 0, 0, 0, 0, 0, 0, 0, 0,
                                25374.8, 0, -25962.2, 0, 0, 0, 0, 0, 0, 0},
                               -1.03793e+16);
  Fieldspace::Point testpoint2({36873.5,  0, 41169.1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                -25374.8, 0, 25962.2, 0, 0, 0, 0, 0, 0, 0},
                               -1.03793e+16);

  auto res1 = test.CalculateBounce(testpoint1);
  auto res2 = test.CalculateBounce(testpoint2);

  REQUIRE(res1.B() == CatchApprox(3291.9397));
  REQUIRE(res2.B() == CatchApprox(3291.9397));
}
