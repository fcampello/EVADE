#include "EVADE/Utilities/Utilities.hpp"
#include "catch.hpp"
#include <limits>

TEST_CASE("RemoveDoubleSlashes", "[unit][util]") {
  using EVADE::Utilities::RemoveDoubleSlashes;

  std::string test("/test/foo//bar////foo/");
  std::string result("/test/foo/bar/foo/");

  REQUIRE(RemoveDoubleSlashes(result) == result);
  REQUIRE(RemoveDoubleSlashes(test) == result);
}

TEST_CASE("IsReal", "[unit][util]") {
  using EVADE::Utilities::IsReal;

  REQUIRE(IsReal({1, 0}));
  REQUIRE_FALSE(IsReal({0, 1}));

  REQUIRE(IsReal({0, 1e-10}));
  REQUIRE_FALSE(IsReal({0, 1e-4}));

  REQUIRE(IsReal({1000, 1e-5}));
  REQUIRE_FALSE(IsReal({100, 1e-2}));

  // output seed in HOM4PS2
  REQUIRE_FALSE(IsReal({0.0000000000000000E+00, -6.1465803456664969E-01}));
  REQUIRE(IsReal({2.0488493622431114E-04, -1.0698947610565432E-07}));
  REQUIRE(IsReal({5.5792900095227224E-05, 1.2043531239413618E-07}));
  REQUIRE_FALSE(IsReal({1.6684000474208958E-05, 4.2784528665746416E-05}));
  REQUIRE_FALSE(IsReal({-6.1720961210331649E-01 ,  3.9804924925411229E-07}));
  REQUIRE_FALSE(IsReal({-8.1126331110035699E-01 , -3.0695848418836371E-07}));
}

TEST_CASE("Normalizers", "[unit][util]") {
  using Catch::Detail::Approx;
  using EVADE::Utilities::Distance;
  using EVADE::Utilities::Norm;
  using EVADE::Utilities::Normalize;
  using EVADE::Utilities::NormalizedDirection;

  SECTION("Norm") {
    REQUIRE(Norm({3, -4}) == Approx(5));
    REQUIRE(Norm({0, 0, 0, 0}) == 0);
  }

  SECTION("Normalize") {
    {
      std::vector<double> vec{1., 2., -3., 4.};
      const std::vector<double> expected{0.182574, 0.365148, -0.547723,
                                         0.730297};
      REQUIRE_NOTHROW(Normalize(vec));
      for (size_t i = 0; i != vec.size(); ++i) {
        REQUIRE(vec[i] == Approx(expected[i]));
      }
    }
    {
      std::vector<double> vec{0., 0., 0., std::numeric_limits<double>::min()};
      REQUIRE_NOTHROW(Normalize(vec));
      REQUIRE(vec == std::vector<double>({0., 0., 0., 0.}));
    }
  }

  SECTION("Distance") {
    std::vector<double> vec1{1., 2., -3., 4.};
    std::vector<double> vec2{-2.8, 1.1, -2., -0.1};
    std::vector<double> vec0(vec1.size(), 0);
    REQUIRE(Distance(vec1, vec2) == Approx(5.74978));
    REQUIRE(Distance(vec1, vec2) == Approx(Distance(vec2, vec1)));
    REQUIRE(Distance(vec1, vec0) == Approx(Norm(vec1)));
    REQUIRE(Distance(vec0, vec2) == Approx(Norm(vec2)));
    REQUIRE(Distance(vec1, vec1) == Approx(0));
  }

  SECTION("Normalized Direction") {
    std::vector<double> vec1{1., 2., -3., 4.};
    std::vector<double> vec2{-2.8, 1.1, -2., -0.1};
    std::vector<double> vec0(vec1.size(), 0);
    {
      auto norm = NormalizedDirection(vec1, vec2);
      auto dist = Distance(vec1, vec2);
      auto norm2 = NormalizedDirection(vec2, vec1);
      std::vector<double> expected{-0.660895, -0.156528, 0.17392, -0.71307};
      for (size_t i = 0; i != norm.size(); ++i) {
        REQUIRE(norm[i] == Approx(expected[i]));
        REQUIRE(vec1[i] + dist * norm[i] == Approx(vec2[i]));
        REQUIRE(-norm[i] == Approx(norm2[i]));
      }
    }
    {
      auto norm = NormalizedDirection(vec0, vec1);
      auto expected = vec1;
      Normalize(expected);
      for (size_t i = 0; i != norm.size(); ++i) {
        REQUIRE(norm[i] == Approx(expected[i]));
      }
    }
    {
      auto norm = NormalizedDirection(vec1, vec1);
      for (size_t i = 0; i != norm.size(); ++i) {
        REQUIRE(norm[i] == Approx(vec0[i]));
      }
    }
  }
}
